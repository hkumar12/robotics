This zipped folder has 8 folders. 

1. The first folder named RoboticsSceneFile holds the VREP scene file for the training and testing scenes. (robotics.ttt -> is used for training and testing1.ttt, testing2.ttt are used for testing)

2. The second folder named SimulationData is just a temporary folder for holding onto the images during testing. (Please do not delete it).

3. The third folder named SourceCode has the acual source code written in python. The file trainingScene.py is the one that we used for training the model by navigating robot in the simulation through keyboard controls. The file testingScene.py is used for testing the model by letting the robot navigate in an unknown setting. The file trainTrafficSigns holds the code for training the model on input images.

** Please note that you have to first start the VREP simulation and then run the individual codes for either training or testing. This folder already holds the multilabel SVM model (files named model.pkl_**.npy). Please use those instead of manually training the model again. The program by default does this. So you just need to run the testingScene file on a VREP sample test scene.

** For example run the scene roboticts.ttt and then the py file trainingScene.py for training the model.
** Run testing1.ttt or testing2.ttt and then the py file testingScene.py for testing the model.

4. The fourth folder named TestingData has images that have been used for testing.

5. The fifth folder TrainingData has the images used for training the model.

6. The sixth folder TrainingDataSamples has the images from the GTSRB dataset that will be used for wrapping around the cuboids.

7. The seventh folder ValidationData has additional images for validating the trained model.

8. The eight folder has video recording of a sample test run on VREP.