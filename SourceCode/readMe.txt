Make sure you have following files in your directory, in order to run the various examples:

1. vrep.py
2. vrepConst.py
3. the appropriate remote API library: "remoteApi.dll" (Windows), "remoteApi.dylib" (Mac) or "remoteApi.so" (Linux)
4. simpleTest.py (or any other example file)

* Run the file vrep_main_python.

* Install all necessary packages :

import vrep                  #V-rep library

import numpy as np         #array library

import matplotlib.pyplot as mpl   #used for image plotting

import pyHook, pythoncom

I used pip in windows, it works good. After installing pip using easy-install (you have to install that too), just go to your python directory and do e.g. pip install numpy etc.

* Start Vrep first before running the program for a successfull connection.

