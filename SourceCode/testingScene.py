#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 05 11:49:44 2015

@author: dell

"""

#Import Libraries:
import vrep                  #V-rep library
import sys
import time                #used to keep track of time
import numpy as np         #array library
import math
import matplotlib.pyplot as mpl   #used for image plotting
import matplotlib.image
import csv
import trainTrafficSigns as train
import os

if sys.platform == "linux" or sys.platform == "linux2":
	import pyxhook.pyxhook as pyHook
elif sys.platform == "win32":
	import pyHook, pythoncom

#import msvcrt
#==============================================================================
# import Tkinter as tk
# 
# def onKeyPress(event):
#     print event.char;
#==============================================================================

left_velocity = 3
right_velocity = 3
train_context = 'left'
counterL = 0
counterR = 0

# Commands are send via the string signal 'commandsFromRemoteApiClient'.
# Commands are simply appended to that string signal
# Each command is coded in following way:
# 1. Command ID (integer, 4 bytes)
# 2. Command counter (integer, 4 bytes). Simply start with 0 and increment for each command you send
# 3. Command length (integer, includes the command ID, the command counter, the command length, and the additional data (i.e. command data))
# 4. Command data (chars/bytes, can be of any length, depending on the command)

def getCmdString(id,cnt,data):
    l=12+len(data)
    retData=vrep.simxPackInts([id,cnt,l])
    return retData+data

def waitForCmdReply(cnt):
    while True:
        result,string=vrep.simxReadStringStream(clientID,'repliesToRemoteApiClient',vrep.simx_opmode_streaming)
        if result==vrep.simx_return_ok:
            while len(string)!=0:
                headerPacked=string[0:12]
                header=vrep.simxUnpackInts(headerPacked)
                if cnt==header[1]:
                    replyData=''
                    if header[2]>12:
                        replyData=string[12:header[2]]
                    return replyData
                string=string[header[2]:len(string)]

def processComand(cmdID, cmdData=b''):
    if not hasattr(processComand, "cmdCnt"):
        processComand.cmdCnt = 0
    dataToSend=getCmdString(cmdID,processComand.cmdCnt,cmdData)
    vrep.simxWriteStringStream(clientID,'commandsFromRemoteApiClient',dataToSend,vrep.simx_opmode_oneshot)
    replyData=waitForCmdReply(processComand.cmdCnt)
    if sys.version_info[0] == 3:
        replyData=str(replyData,'utf-8')
    print ('Return string: ',replyData) 
    processComand.cmdCnt += 1

# Helps in making the robot click images every second and store them for prediction by the model
def clickImage():
    global counterL, counterR
    returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID,cam_handle,0,vrep.simx_opmode_buffer)
    im = np.array(image, dtype=np.uint8)
    im.resize([resolution[0],resolution[1],3])
    matplotlib.image.imsave('E:\\ASU\\Course Files\\Fall 2015\\Robotics\\Project\\SimulationData\\image.jpg', im, origin='lower')

# rotate 90 clockwise/anticlockwise
def rotate90(angle):
    global left_velocity, right_velocity
    if angle == 90:
        t = time.time()
        left_velocity = 0.5
        right_velocity = 1
        while (time.time()-t) < 7:
            move()
    elif angle == -90:
        t = time.time()
        left_velocity = 1
        right_velocity = 0.5
        while (time.time()-t) < 7:
            move()

# method for making the robot move
def move():
    global left_velocity, right_velocity
    returnCode=vrep.simxSetJointTargetVelocity(clientID,left_motor_handle,left_velocity, vrep.simx_opmode_streaming)
    returnCode=vrep.simxSetJointTargetVelocity(clientID,right_motor_handle,right_velocity, vrep.simx_opmode_streaming)
    return returnCode

# method for making the robot stop
def stop():
    global left_velocity, right_velocity
    left_velocity = 0
    right_velocity = 0
    move();
    return returnCode
 
def loadScene(path):
    returnCode=vrep.simxLoadScene(clientID,path,0,vrep.simx_opmode_oneshot_wait)
    print returnCode

def setUIdata(label, data):
    if label == '1':
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,2,str(data),"",vrep.simx_opmode_oneshot)
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,3,"NULL","",vrep.simx_opmode_oneshot)
    elif label == '2':
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,3,str(data),"",vrep.simx_opmode_oneshot)
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,2,"NULL","",vrep.simx_opmode_oneshot)
    else:
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,3,"NULL","",vrep.simx_opmode_oneshot)
        returnCode=vrep.simxSetUIButtonLabel(clientID,ui_handle,2,"NULL","",vrep.simx_opmode_oneshot)    


#Pre-Allocation

PI=math.pi  #pi=3.14..., constant

vrep.simxFinish(-1) # just in case, close all opened connections

clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5)

if clientID!=-1:  #check if client connection successful
    print 'Connected to remote API server'
    
else:
    print 'Connection not successful'
    sys.exit('Could not connect')

returnCode,ui_handle=vrep.simxGetUIHandle(clientID,"UI",vrep.simx_opmode_oneshot_wait)

#retrieve motor  handles
errorCode,left_motor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor',vrep.simx_opmode_oneshot_wait)
errorCode,right_motor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor',vrep.simx_opmode_oneshot_wait)


sensor_h=[] #empty list for handles
sensor_val=np.array([]) #empty array for sensor measurements

#orientation of all the sensors: 
sensor_loc=np.array([-PI/2, -50/180.0*PI,-30/180.0*PI,-10/180.0*PI,10/180.0*PI,30/180.0*PI,50/180.0*PI,PI/2,PI/2,130/180.0*PI,150/180.0*PI,170/180.0*PI,-170/180.0*PI,-150/180.0*PI,-130/180.0*PI,-PI/2]) 

#for loop to retrieve sensor arrays and initiate sensors
for x in range(4,6):
        errorCode,sensor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor'+str(x),vrep.simx_opmode_oneshot_wait)
        sensor_h.append(sensor_handle) #keep list of handles        
        errorCode,detectionState,detectedPoint,detectedObjectHandle,detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor_handle,vrep.simx_opmode_streaming)                
        sensor_val=np.append(sensor_val,np.linalg.norm(detectedPoint)) #get list of values
        
t = time.time()

errorCode,cam_handle = vrep.simxGetObjectHandle(clientID,'cam1',vrep.simx_opmode_oneshot_wait)
returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID,cam_handle,0,vrep.simx_opmode_streaming)

move();

root_path = os.path.dirname(os.path.dirname(os.path.realpath('__file__')))
simulationTestScene_path = os.path.join(root_path,"SimulationData")

while True:

    sensor_val=np.array([])

    returnCode = 1
    while returnCode !=0:
        returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID,cam_handle,0,vrep.simx_opmode_buffer);
            
    im = np.array(image, dtype=np.uint8)
    im.resize([resolution[0],resolution[1],3])
    matplotlib.image.imsave(os.path.join(simulationTestScene_path,"image.jpg"), im, origin='lower')

    prob,label = train.callGetPredProb(os.path.join(simulationTestScene_path,"image.jpg"))
   
    setUIdata(label[0],int((prob[0][label[0]] * 100) + 0.5) / 100.0)
    
    for x in range(0,2):
        errorCode,detectionState,detectedPoint,detectedObjectHandle,detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor_h[x],vrep.simx_opmode_buffer)                
        sensor_val=np.append(sensor_val,np.linalg.norm(detectedPoint)) #get list of values
  
    max_ind=np.where(sensor_val==np.max(sensor_val))
    max_ind=max_ind[0][0]
      
    if sensor_val[max_ind]>=0.9:
        stop();
      
        if label[0] == '1':
                rotate90(90);
                left_velocity = 3
                right_velocity = 3
                move();
        elif label[0] == '2':
                rotate90(-90);
                left_velocity = 3
                right_velocity = 3
                move();

