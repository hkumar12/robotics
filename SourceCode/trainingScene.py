#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 05 11:49:44 2015

@author: dell

"""

#Import Libraries:
import vrep                  #V-rep library
import sys
import time                #used to keep track of time
import numpy as np         #array library
import math
import matplotlib.pyplot as mpl   #used for image plotting
import matplotlib.image
import csv
import trainTrafficSigns as train
import re
import os

if sys.platform == "linux" or sys.platform == "linux2":
	import pyxhook.pyxhook as pyHook
elif sys.platform == "win32":
	import pyHook, pythoncom

#import msvcrt
#==============================================================================
# import Tkinter as tk
# 
# def onKeyPress(event):
#     print event.char;
#==============================================================================

left_velocity = 3
right_velocity = 3
train_context = 'left'
counterL = 0
counterR = 0
testFlag = True

# Commands are send via the string signal 'commandsFromRemoteApiClient'.
# Commands are simply appended to that string signal
# Each command is coded in following way:
# 1. Command ID (integer, 4 bytes)
# 2. Command counter (integer, 4 bytes). Simply start with 0 and increment for each command you send
# 3. Command length (integer, includes the command ID, the command counter, the command length, and the additional data (i.e. command data))
# 4. Command data (chars/bytes, can be of any length, depending on the command)

def getCmdString(id,cnt,data):
    l=12+len(data)
    retData=vrep.simxPackInts([id,cnt,l])
    return retData+data

def waitForCmdReply(cnt):
    while True:
        result,string=vrep.simxReadStringStream(clientID,'repliesToRemoteApiClient',vrep.simx_opmode_streaming)
        if result==vrep.simx_return_ok:
            while len(string)!=0:
                headerPacked=string[0:12]
                header=vrep.simxUnpackInts(headerPacked)
                if cnt==header[1]:
                    replyData=''
                    if header[2]>12:
                        replyData=string[12:header[2]]
                    return replyData
                string=string[header[2]:len(string)]

def processComand(cmdID, cmdData=b''):
    if not hasattr(processComand, "cmdCnt"):
        processComand.cmdCnt = 0
    dataToSend=getCmdString(cmdID,processComand.cmdCnt,cmdData)
    vrep.simxWriteStringStream(clientID,'commandsFromRemoteApiClient',dataToSend,vrep.simx_opmode_oneshot)
    replyData=waitForCmdReply(processComand.cmdCnt)
    if sys.version_info[0] == 3:
        replyData=str(replyData,'utf-8')
    #print ('Return string: ',replyData) 
    processComand.cmdCnt += 1

# method for making the robot move
def move():
    global left_velocity, right_velocity
    returnCode=vrep.simxSetJointTargetVelocity(clientID,left_motor_handle,left_velocity, vrep.simx_opmode_streaming)
    returnCode=vrep.simxSetJointTargetVelocity(clientID,right_motor_handle,right_velocity, vrep.simx_opmode_streaming)
    return returnCode

# method for making the robot stop
def stop():
    global left_velocity, right_velocity
    left_velocity = 0
    right_velocity = 0
    move();
    return returnCode

# method for making the robot take a left turn
def left_turn():
    global left_velocity, right_velocity
    left_velocity -= 1
    right_velocity += 1
    move();
    return returnCode

#method for making the robot take a right turn
def right_turn():
    global left_velocity, right_velocity
    left_velocity += 1
    right_velocity -= 1
    move();
    return returnCode

# method for reversing the robot
def reverse():
    global left_velocity, right_velocity
    left_velocity = -2
    right_velocity = -2
    move();
    return returnCode

# method for clicking the images through the robots front camera
def clickImage():
    global counterL, counterR, counterV
    returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID,cam_handle,0,vrep.simx_opmode_buffer)
    im = np.array(image, dtype=np.uint8)
    im.resize([resolution[0],resolution[1],3])
    if train_context == 'left':
            matplotlib.image.imsave(os.path.join(trainingData_path, '000\\left%s.jpg') % counterL, im, origin='lower')
            data = [['left%s'%counterL,'0']]
            with open(os.path.join(trainingData_path, '000\\GT-000.csv'), 'ab') as fp:
                a = csv.writer(fp)
                a.writerows(data)
            counterL = counterL + 1;
    elif train_context == 'right':
            matplotlib.image.imsave(os.path.join(trainingData_path, '001\\right%s.jpg') % counterR, im, origin='lower')
            data = [['right%s'%counterR,'1']]
            with open(os.path.join(trainingData_path, '001\\GT-001.csv'), 'ab') as fp:
                a = csv.writer(fp)
                a.writerows(data)
            counterR = counterR + 1;
    
# method for rotating the robot by 90 degree clockwise/anticlockwise
def rotate90(angle):
    global left_velocity, right_velocity
    if angle == 90:
        t = time.time()
        left_velocity = 0.5
        right_velocity = 1
        while (time.time()-t) < 7.2:
            move()
    elif angle == -90:
        t = time.time()
        left_velocity = 1
        right_velocity = 0.5
        while (time.time()-t) < 7.2:
            move()

def loadScene(path):
    returnCode=vrep.simxLoadScene(clientID,path,0,vrep.simx_opmode_oneshot_wait)
    #print returnCode

def changeModel(model):
    global currentModelHandle
    returnCode=vrep.simxRemoveModel(clientID,currentModelHandle, vrep.simx_opmode_oneshot)
    returnCode,currentModelHandle=vrep.simxLoadModel(clientID,os.path.join(model_path, model),1,vrep.simx_opmode_oneshot_wait)

def resetRobot():
    stop()
    returnCode=vrep.simxSetObjectPosition(clientID,robot_handle,-1,robot_position,vrep.simx_opmode_oneshot)
    returnCode=vrep.simxSetObjectOrientation(clientID,robot_handle,-1,robot_oriantation,vrep.simx_opmode_oneshot)

# Mapping of keyboard controls to make the robot move, reverse, stop, take pictures etc
def OnKeyboardEvent(event):
    global left_velocity, right_velocity, train_context, testFlag
    keypressed =  chr(event.Ascii)
    if keypressed == 'a':
        left_turn();
    elif keypressed == 'd':
        right_turn();
    elif keypressed == 'w':
        left_velocity = 4
        right_velocity = 4
        move();
    elif keypressed == 's':
        left_velocity = 4
        right_velocity = 4
        move();
    elif keypressed == 'e':
        stop();
    elif keypressed == 'r':
        reverse();
    elif keypressed == 'c':
        clickImage();
    elif keypressed == 'j':
        train_context = 'left'
        processComand(2,str(1))
    elif keypressed == 'l':
        train_context = 'right'
        processComand(2,str(2))
    elif keypressed == 'k':
        processComand(1,'random')
    elif keypressed == '[':
        changeModel("training_model.ttm")
        resetRobot()
    elif keypressed == ']':
        changeModel("testing_model_simple.ttm")
        resetRobot()
        testScene()
    elif keypressed == 'b':
        testFlag = False
    elif keypressed == 't':
        rmse = train.callTrainImages();
        vrep.simxAddStatusbarMessage(clientID, "The accuracy on training set is: " + str(100*(1-rmse)), vrep.simx_opmode_oneshot);
    return True

#Pre-Allocation
PI=math.pi  #pi=3.14..., constant

# Paths
root_path = os.path.dirname(os.path.dirname(os.path.realpath('__file__')))
model_path = os.path.join(root_path,"RoboticsSceneFile\models")
trainingData_path = os.path.join(root_path,"TrainingData")
simulationTestScene_path = os.path.join(root_path,"SimulationData")

vrep.simxFinish(-1) # just in case, close all opened connections

clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5)

if clientID!=-1:  #check if client connection successful
    print 'Connected to remote API server'
    
else:
    print 'Connection not successful'
    sys.exit('Could not connect')

#Getting Robot Start orientation
returnCode,robot_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx',vrep.simx_opmode_oneshot_wait)
returnCode,robot_position=vrep.simxGetObjectPosition(clientID,robot_handle,-1,vrep.simx_opmode_streaming)
while returnCode!=0:
    returnCode,robot_position=vrep.simxGetObjectPosition(clientID,robot_handle,-1,vrep.simx_opmode_buffer)
returnCode,robot_oriantation=vrep.simxGetObjectOrientation(clientID,robot_handle,-1,vrep.simx_opmode_streaming)
while returnCode!=0:
    returnCode,robot_oriantation=vrep.simxGetObjectOrientation(clientID,robot_handle,-1,vrep.simx_opmode_buffer)

#Loading Environment
returnCode,currentModelHandle=vrep.simxLoadModel(clientID,os.path.join(model_path, "training_model.ttm"),1,vrep.simx_opmode_oneshot_wait)

#retrieve motor  handles
returnCode,left_motor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor',vrep.simx_opmode_oneshot_wait)
returnCode,right_motor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor',vrep.simx_opmode_oneshot_wait)

t = time.time()

returnCode,cam_handle = vrep.simxGetObjectHandle(clientID,'cam1',vrep.simx_opmode_oneshot_wait)

returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID,cam_handle,0,vrep.simx_opmode_streaming)

if os.path.isfile(os.path.join(trainingData_path, '000\\GT-000.csv')):
        with open(os.path.join(trainingData_path, '000\\GT-000.csv'), 'r') as fpRead:
                reader = csv.reader(fpRead)
                for line in reader:
                        lastLine = line
        string = lastLine[0]        
        counterL = int(re.findall(r'\d+', string)[0])
        counterL = counterL + 1

if os.path.isfile(os.path.join(trainingData_path, '001\\GT-001.csv')):
        with open(os.path.join(trainingData_path, '001\\GT-001.csv'), 'r') as fpRead:
                reader = csv.reader(fpRead)
                for line in reader:
                        lastLine = line
        string = lastLine[0]        
        counterR = int(re.findall(r'\d+', string)[0])
        counterR = counterR + 1

hooks_manager = pyHook.HookManager()
hooks_manager.KeyDown = OnKeyboardEvent
hooks_manager.HookKeyboard()
if sys.platform == "linux" or sys.platform == "linux2":
	hooks_manager.start()
elif sys.platform == "win32":
	pythoncom.PumpMessages()

while (time.time()-t)<60:
	time.sleep(0.1)
    
hooks_manager.cancel()
#Post ALlocation
stop();
