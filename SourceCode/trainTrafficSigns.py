# The German Traffic Sign Recognition Benchmark
#
# sample code for reading the traffic sign images and the
# corresponding labels
#
# example:
#            
# trainImages, trainLabels = readTrafficSigns('GTSRB/Training')
# print len(trainLabels), len(trainImages)
# plt.imshow(trainImages[42])
# plt.show()
#
# have fun, Christian

from __future__ import division

import matplotlib.pyplot as plt
import csv
#from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsRestClassifier
from sklearn.calibration import CalibratedClassifierCV
#from sklearn.svm import SVC
import numpy as np
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from math import sqrt
import os
from sklearn.svm import SVC

totalSamples = 0

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.144])

# function for reading the images
# arguments: path to the traffic sign data, for example './GTSRB/Training'
# returns: list of images, list of corresponding labels 
def readTrafficSigns(rootpath):
    '''Reads traffic sign data for German Traffic Sign Recognition Benchmark.

    Arguments: path to the traffic sign data, for example './GTSRB/Training'
    Returns:   list of images, list of corresponding labels'''
    images = [] # images
    labels = [] # corresponding labels
    samples = 0
    global totalSamples
    # loop over all 42 classes
    for c in range(0,3):
        prefix = rootpath + '\\' + format(c, '03d') + '\\' # subdirectory for class
        gtFile = open(prefix + 'GT-'+ format(c, '03d') + '.csv') # annotations file
        gtReader = csv.reader(gtFile, delimiter=',') # csv parser for annotations file
        for row in gtReader:
            label = row[1]
            rgb = rgb2gray(plt.imread(prefix + row[0] + ".jpg"))
            rgb = np.asarray(rgb)
            images.append(rgb.ravel())
            samples = samples + 1
            totalSamples = totalSamples + 1
        for x in range(0, samples):
            labels.append(label)
        samples = 0
        gtFile.close()
    return images, labels

def readTestSigns(rootpath):
    '''Reads traffic sign data for German Traffic Sign Recognition Benchmark.

    Arguments: path to the traffic sign data, for example './GTSRB/Training'
    Returns:   list of images, list of corresponding labels'''
    images = [] # images
    labels = [] # corresponding labels
    samples = 0
    global totalSamples
    # loop over all 42 classes
    for c in range(0,1):
        prefix = rootpath + '\\' # subdirectory for class
        gtFile = open(prefix + 'GT-'+ format(c, '03d') + '.csv') # annotations file
        gtReader = csv.reader(gtFile, delimiter=',') # csv parser for annotations file
        for row in gtReader:
            label = row[1]
            rgb = rgb2gray(plt.imread(prefix + row[0] + ".jpg"))
            rgb = np.asarray(rgb)
            images.append(rgb.ravel())
            samples = samples + 1
            totalSamples = totalSamples + 1
            labels.append(label)
        samples = 0
        gtFile.close()
    return images, labels

# for the multi label classification model
def trainImages():
    print 'training starts...'
    images, labels = readTrafficSigns(trainingData_path)
    images = np.asarray(images)
    images = np.reshape(images, (totalSamples,16384))
    labels = np.asarray(labels)
    model = OneVsRestClassifier(SVC(kernel='linear', probability=True, class_weight='balanced'))
    model.fit(images, labels)
    print 'saving the model...'
    joblib.dump(model, 'model.pkl')
    print 'done saving the model..'
    print 'training ends...'
    return model
    
def fitModel(model,testImages):
    predictedLabels = model.predict(testImages)
    return predictedLabels

# for the binary classification model
def callTrainImages():
    global totalSamples
    images, labels = readTrafficSigns(trainingData_path)
    images = np.asarray(images)
    labels = np.asarray(labels)
    if os.path.isfile('model.pkl'):
        model = joblib.load('model.pkl')
    else:
        print 'training starts...'
        model = SVC(C=1, kernel='linear', probability = True)
        model.fit(images, labels)
        print 'training ends...'
        print 'saving the model...'
        joblib.dump(model, 'model.pkl')
        print 'done saving the model..'
    print 'testing on train set starts...'
    predictedLabels = model.predict(images)
    print 'testing on train set ends...'
    print 'accuracy in prediction...'
    indices = []
    counter = 0
    for x,y in np.c_[predictedLabels.astype(np.float),labels.astype(np.float)]:
        if x == y:
            indices.append(counter)
        counter = counter + 1;
    accuracy = len(indices)/(labels.size)
    print accuracy
    return accuracy*100

def callGetPredProb(image):
    if os.path.isfile('model.pkl'):
        model = joblib.load('model.pkl')
    rgb = rgb2gray(plt.imread(image))
    rgb = np.asarray(rgb)
    rgb = np.reshape(rgb, (1,16384))
    prob = model.predict_proba(rgb)
    label = model.predict(rgb)
    return (prob,label)

def getTestSetPredProb():
    countL = 0
    countR = 0
    if os.path.isfile('model.pkl'):
        model = joblib.load('model.pkl')
    images, labels = readTestSigns(TestingData_path)
    predictedLabels = model.predict(images)
    labels = np.asarray(labels)
    indices = []
    counter = 0
    for x,y in np.c_[predictedLabels.astype(np.float),labels.astype(np.float)]:
        if x == 1 and y == 1:
            countL = countL + 1
            indices.append(counter)
        elif x == 2 and y ==2:
            countR = countR + 1
        counter = counter + 1;
    accuracy = len(indices)/(labels.size)
    print indices
    print countL
    print countR
    return (accuracy*100,predictedLabels)
    
#Uncomment the below lines if you manually want to train the model

#root_path = os.path.dirname(os.path.dirname(os.path.realpath('__file__')))
#TestingData_path = os.path.join(root_path,"TestingData")
#trainingData_path = os.path.join(root_path,"TrainingData")
#trainImages()
#accuracy = callTrainImages()
#print 'Accuracy on training set: '
#print accuracy
#accuracy, labels = getTestSetPredProb()
#print 'Accuracy on the test set: '
#print accuracy
#print labels



