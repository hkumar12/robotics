DEG_90 = math.pi/2

--http://www.coppeliarobotics.com/helpFiles/en/apiFunctions.htm#simCreateTexture
--http://www.coppeliarobotics.com/helpFiles/en/apiFunctions.htm#simSetShapeTexture
textureProperties = {
	mappingMode=sim_texturemap_plane,
	--options
	interpolated=1,
	decal=1,
	repeat_U=0,
	repeat_V=0,
	option,
	----------
	planeSizes=NULL,
	uvScaling={.75,.75},
	position=NULL,
	fixedResolution=0,
	----------
	orientations = {{DEG_90,0,0},{0,DEG_90,DEG_90}
}}

textureProperties.option = textureProperties.interpolated 
						 + textureProperties.decal*2 
						 + textureProperties.repeat_U*4 
						 + textureProperties.repeat_V*8

-- images from: https://www.gov.uk/guidance/traffic-sign-images
rootdir="C:\\Users\\Indranil\\Documents\\ASU_Sem3\\CSE591_Robotics\\robotics\\TestingDataSamples\\"
rootdir="C:\\Users\\Indranil\\Documents\\ASU_Sem3\\CSE591_Robotics\\robotics\\TestingDataSamples\\"
dirtoscan = {
{rootdir .. "LeftTurn\\", {}},
{rootdir .. "RightTurn\\", {}},
--{rootdir .. "information-signs-jpg/", {}},
--{rootdir .. "level-crossing-signs-jpg/", {}},
--{rootdir .. "low-bridge-signs-jpg/", {}},
--{rootdir .. "miscellaneous-jpg/", {}},
--{rootdir .. "motorway-signs-jpg/", {}},
--{rootdir .. "on-street-parking-jpg/", {}},
--{rootdir .. "pedestrian,-cycle,-equestrian-jpg/", {}},
--{rootdir .. "pedestrian-zone-signs-jpg/", {}},
--{rootdir .. "regulatory-signs-jpg/", {}},
--{rootdir .. "road-works-and-temporary-jpg/", {}},
--{rootdir .. "signs-for-cyclists-and-pedestrians-jpg/", {}},
--{rootdir .. "speed-limit-signs-jpg/", {}},
--{rootdir .. "tidal-flow-lane-control-jpg/", {}},
--{rootdir .. "traffic-calming-jpg/", {}},
--{rootdir .. "tram-signs-jpg/", {}}
--{rootdir .. "warning-signs-jpg/", {}}
}

function scandir(directory)
	--print(directory)
    local i, t, popen = 0, {}, io.popen
    for filename in popen('dir "'..directory..'" /b *.ppm'):lines() do
    --for filename in popen('ls "'..directory..'" *.ppm'):lines() do
        i = i + 1
        t[i] = filename
		--print(filename)
    end
    return t
end

function getfiles()
	for dirs = 1, #dirtoscan do
		dir = dirtoscan[dirs][1]
		--print(dir)
		dirtoscan[dirs][2] = scandir(dir)
		--print(#dirtoscan[dirs][2])
		--if (dirtoscan[dirs][2]~=nil) then
			--for count = 1, #dirtoscan[dirs][2] do
				--print (dirtoscan[dirs][2][count])
			--end
		--end
	end
end

function getRandFile(folderNo)
	local x
	if folderNo == nil then
		x = math.random(1,#dirtoscan)
	else
		x = tonumber(folderNo)
	end
	local y = math.random(1,#dirtoscan[x][2])

	local file = dirtoscan[x][1] .. dirtoscan[x][2][y]
	return file
end

function overlayTexture(shapeHandle, textureFile)
	--print(textureFile)
	textureHandle, textureId = simCreateTexture(textureFile, textureProperties.option, 
									textureProperties.planeSizes, textureProperties.uvScaling, 
									nil, textureProperties.fixedResolution, nil)
	if (textureId~=-1) then
		simSetShapeTexture(shapeHandle, textureId, textureProperties.mappingMode, 
							textureProperties.option, textureProperties.uvScaling, 
							--nil, textureProperties.orientations[1])
							nil, nil)
	end
	simRemoveObject(textureHandle)
end

function overlayALL(folderNo) 
	i=0
	handle=simGetObjectHandle("Cuboid")
	while (handle~=-1) do
		overlayTexture(handle,getRandFile(folderNo))
		handle=simGetObjectHandle("Cuboid" .. i)
		i = i + 1
	end
end

threadFunction=function()
   while simGetSimulationState()~=sim_simulation_advancing_abouttostop do
      local commands=simGetStringSignal('commandsFromRemoteApiClient') -- Read commands sent from a remote API client
      if commands then
         simClearStringSignal('commandsFromRemoteApiClient') -- Clear the signal
         -- Process the commands in following loop:
         while #commands>0 do
            local cmdID=simUnpackInts(commands,0,1)[1]
            local cmdCounter=simUnpackInts(commands,1,1)[1]
            local cmdLength=simUnpackInts(commands,2,1)[1]
            local cmdData=''
            if cmdLength>12 then
               cmdData=string.sub(commands,13,13+cmdLength-12)
            end
            commands=string.sub(commands,cmdLength+1) -- this contains the next commands

            -- Now process the command, and prepare a reply string signal:
            local reply=''
            if cmdID==1 then
			   overlayALL(nil)
               reply='Done !!!'
            elseif cmdID==2 then
			   overlayALL(cmdData)
               reply='Done !!!'
            end
    
            -- Now, before setting up the reply string, append the cmdID, and a reply length:
            local replyLength=12+#reply
            local replyFromOtherCommands=simGetStringSignal('repliesToRemoteApiClient')
            if not replyFromOtherCommands then
               replyFromOtherCommands=''
            end
            local totalReplySignal=replyFromOtherCommands..simPackInts({cmdID})..simPackInts({cmdCounter})..simPackInts({replyLength})..reply
            simSetStringSignal('repliesToRemoteApiClient',totalReplySignal) -- update the reply signal
         end
      end
      simSwitchThread()
   end
end

-- Put some initialization code here:
simExtRemoteApiStart(19999)
simSetThreadSwitchTiming(2) -- Default timing for automatic thread switching
math.randomseed(os.time())
math.random(); math.random(); math.random()

-- local lfs = require( "lfs" )
-- print (lfs.currentdir())

getfiles()
--overlayALL() 

-- Here we execute the regular thread code:
res,err=xpcall(threadFunction,function(err) return debug.traceback(err) end)
if not res then
	simAddStatusbarMessage('Lua runtime error: '..err)
end

-- Put some clean-up code here:
